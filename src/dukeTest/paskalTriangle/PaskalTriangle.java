package dukeTest.paskalTriangle;

public class PaskalTriangle
{
	private int size;

	private int[][] matrix;

	public PaskalTriangle(int n)
	{
		this.size = n;
		this.matrix = new int[n][n + 1];
	}

	public void constructMatrix()
	{
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j <= i + 1; j++)
			{
				matrix[i][j] = defineValue(i, j);

				if (matrix[i][j] != 0)
				{
					System.out.printf("%5d", matrix[i][j]);
				}
			}

			System.out.println();
		}
	}

	private int defineValue(int i, int j)
	{
		int resultValue = -1;

		if (j < 2)
		{
			resultValue = j;
		}
		else
		{
			resultValue = matrix[i - 1][j - 1] + matrix[i - 1][j];
		}

		return resultValue;
	}

	public static void main(String[] args)
	{

		PaskalTriangle instance = new PaskalTriangle(10);

		instance.constructMatrix();
	}
}
