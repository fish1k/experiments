package dukeTest.shternBrokko;

import java.util.Comparator;

public class ShternBrokko
{
	private int targetNominator;
	private int targetDenominator;

	private final Fraction LEFT = new Fraction(0, 1);
	private final Fraction MIDDLE = new Fraction(1, 1);
	private final Fraction RIGHT = new Fraction(1, 0);

	private Fraction targetFraction;
	private Fraction currentFraction;

	public ShternBrokko(int targetNominator, int targetDenominator)
	{
		this.targetNominator = targetNominator;
		this.targetDenominator = targetDenominator;

		this.targetFraction = new Fraction(targetNominator, targetDenominator);
	}

	public void printAsString() throws Exception
	{
		StringBuilder sb = new StringBuilder();

		if (targetNominator == targetDenominator)
		{
			if (targetDenominator == 1)
			{
				sb.append("");
				System.out.println(sb.toString());
				return;
			}
			else
			{
				throw new IllegalArgumentException(String.format("%s, %s",
						targetNominator, targetDenominator));
			}
		}

		if (targetNominator > targetDenominator)
		{
			currentFraction = new Fraction(MIDDLE, MIDDLE, RIGHT);
		}
		else
		{
			currentFraction = new Fraction(MIDDLE, LEFT, MIDDLE);
		}

		while (currentFraction.compareTo(targetFraction) != 0)
		{
			if (targetFraction.compareTo(currentFraction) > 0)
			{
				currentFraction.moveRight();
				sb.append("R");
			}
			else
			{
				currentFraction.moveLeft();
				sb.append("L");
			}
		}

		System.out.println(sb.toString());
	}

	private class Fraction implements Comparable<Fraction>, Cloneable
	{
		private int nominator;
		private int denominator;

		private Fraction leftSibling;
		private Fraction rightSibling;

		public Fraction(int nominator, int denominator)
		{
			this.nominator = nominator;
			this.denominator = denominator;
		}

		public Fraction(Fraction template, Fraction leftSibling,
				Fraction rightSibling)
		{
			this.nominator = template.getNominator();
			this.denominator = template.getDenominator();

			this.leftSibling = leftSibling;
			this.rightSibling = rightSibling;
		}

		public void moveLeft() throws Exception
		{
			this.rightSibling = this.clone();

			this.nominator += leftSibling.getNominator();
			this.denominator += leftSibling.getDenominator();
		}

		public void moveRight() throws Exception
		{
			this.leftSibling = this.clone();

			this.nominator += rightSibling.getNominator();
			this.denominator += rightSibling.getDenominator();
		}

		public int getNominator()
		{
			return nominator;
		}

		public int getDenominator()
		{
			return denominator;
		}

		public double countAbsoluteValue()
		{
			double result = (double) nominator / denominator;

			return result;
		}

		@Override
		protected Fraction clone() throws CloneNotSupportedException
		{
			return (Fraction) super.clone();
		}

		@Override
		public int compareTo(Fraction o)
		{
			double result = this.countAbsoluteValue() - o.countAbsoluteValue();

			if (result > 0)
			{
				return 1;
			}

			if (result < 0)
			{
				return -1;
			}

			return 0;
		}
	}

	public static void main(String[] args)
	{
		try
		{
			ShternBrokko instance = new ShternBrokko(878, 323);

			instance.printAsString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
