package main;

public interface ITestDeprecation
{
	/**
	 * @deprecated Use another method
	 */
	@Deprecated
	void test();
}
